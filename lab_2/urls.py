from lab_2.models import Note
from django.urls import path
from .views import index, Notes, xml, json

urlpatterns = [
    path('', index, name='index'),
    path('notes', Notes, name='friends_list'),
    path('xml', xml, name='xml'),
    path('json', json, name='json'),

]