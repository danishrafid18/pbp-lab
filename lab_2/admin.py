from lab_2.models import Note
from django.contrib import admin
from .models import Note

admin.site.register(Note)

# Register your models here.
