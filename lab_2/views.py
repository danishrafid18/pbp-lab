from django.http import response
from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

to = "Danish"
From = "Rafid"
title = "Test"
message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin diam eget ante sagittis porta quis non orci. Fusce sapien dolor, egestas eu tempor at, condimentum in nisl. Suspendisse ut pellentesque nibh. Etiam nec viverra turpis. Nullam nisi mi, viverra sed tempus at, aliquam a justo. Aenean hendrerit nunc ut auctor porttitor. Aliquam erat volutpat. Nulla facilisi. Etiam quis tincidunt turpis. Proin ornare dignissim nulla. Donec venenatis aliquet nunc, quis sodales neque dignissim vel."


# Create your views here.
def index(request):
    response = {'to': to,
                'From': From,
                'title': title,
                'message': message}
    return render(request, 'index_lab2.html', response)

def Notes(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

    