from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
# Create your views here.
to = "Danish"
From = "Rafid"
title = "Test"
message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin diam eget ante sagittis porta quis non orci. Fusce sapien dolor, egestas eu tempor at, condimentum in nisl. Suspendisse ut pellentesque nibh. Etiam nec viverra turpis. Nullam nisi mi, viverra sed tempus at, aliquam a justo. Aenean hendrerit nunc ut auctor porttitor. Aliquam erat volutpat. Nulla facilisi. Etiam quis tincidunt turpis. Proin ornare dignissim nulla. Donec venenatis aliquet nunc, quis sodales neque dignissim vel."


def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    else:
        form = NoteForm()
        print("masuk ke else")
    return render(request, 'lab4_form.html', {'form': form})


def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)