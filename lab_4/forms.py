from django import forms
from lab_2.models import Note
class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        
        widget = {
            'to': forms.TextInput(attrs={'class': 'form-control'}),
            'From': forms.TextInput(attrs={'class': 'form-control'}),
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'message': forms.Textarea(attrs={'class': 'form-control'}),
        }
    #to = forms.CharField(label='to')
    #From = forms.CharField(label='from')
    #title = forms.CharField(label='title')
    #message = forms.TimeField(label='message')