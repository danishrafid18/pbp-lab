1.) JSON is short for JavaScript Object Notation, while XML is short for eXtensible Markup Language. 
The main differences between JSON and XML are: 

- JSON object features a type while XML data is typeless
- JSON is supported by most browsers while Cross-browser XML parsing can be tricky
- in JSON Retrieving value is straightforward while XML Retrieving value is difficult

Advantages of using JSON:
- Provide support for all browsers
- Easy to read and write
- Straightforward syntax
- You can natively parse in JavaScript using eval() function
- Easy to make and manipulate

Advantages of using XML:
- Makes documents transportable across systems and applications. 
  With the assistance of XML, you'll exchange data quickly between different platforms.
- XML separates the data from HTML
- XML simplifies platform change process
- Allows creating user-defined tags

https://www.guru99.com/json-vs-xml-difference.html


2.) HTML stands for Hypertext Markup Language
The differences between HTML and XML are:

- HTML Focusses on the appearance of data, while The main purpose of XML is to specialise in the transport of data and saving the data
- HTML is static because its main function is within the display of data, 
while XML is dynamic because it is utilized in the transport of data

The main difference between HTML and XML is that HTML displays data and describes the structure of a webpage, 
whereas XML stores and transmits data.

https://byjus.com/free-ias-prep/difference-between-xml-and-html/











