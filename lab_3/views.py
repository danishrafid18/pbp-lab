from django.shortcuts import render
from .forms import NameForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect



# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().order_by('name') # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        #new_name = form["your_name"].value()
        #new_dob = form["DOB"].value()
        #new_npm = form["npm"].value()
        #print(new_name + " " + new_dob + " " + new_npm)
        #new_instance = Friend(name = new_name, DOB = new_dob, NPM = new_npm)
        #form.save()
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')

        # check whether it's valid:
    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()
        print("masuk ke else")

    return render(request, 'lab3_form.html', {'form': form})
