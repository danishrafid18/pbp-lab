from django.urls import path
from .views import friend_list, index, get_name

urlpatterns = [
    path('', index, name='index'),
    path('friends/', friend_list, name='friends_list'),
    path('forms', get_name, name="get_name")
    # TODO Add friends path using friend_list Views
]
