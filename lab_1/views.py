from time import strftime
from django.db.models.expressions import Value
from django.shortcuts import render
from datetime import datetime, date
from .models import Friend
from .forms import NameForm

mhs_name = "Danish Rafid Rajendra"  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 8, 1) # TODO Implement this, format (Year, Month, Date)
npm = "2006489060"  # TODO Implement this

def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all().order_by('name') # TODO Implement this
    response = {'friends': friends}
    return render(request, 'friends_list_lab1.html', response)


def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        name = form["your_name"].value()
        dob = form["DOB"].value()
        npm = form["npm"].value()
        print(name + " " + dob + " " + npm)
        new_instance = Friend(name = name, DOB = dob, NPM = npm)
        new_instance.save()
        # check whether it's valid:

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()
        print("masuk ke else")

    return render(request, 'name.html', {'form': form})
