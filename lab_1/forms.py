from django import forms

class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)
    DOB = forms.DateField(label='Date of birth')
    npm = forms.CharField(label='npm')